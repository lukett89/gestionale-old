import { Action } from '@ngrx/store';

export const OPEN_SIDEBAR =   '[Layout] Open Sidebar';
export const CLOSE_SIDEBAR =  '[Layout] Close Sidebar';
export const TOGGLE_SIDEBAR =  '[Layout] Toggle Sidebar';
export const OPEN_MODAL =  '[Layout] Open Modal';
export const CLOSE_MODAL =  '[Layout] Close Modal';


export class OpenSidebarAction implements Action {
  readonly type = OPEN_SIDEBAR;
}

export class CloseSidebarAction implements Action {
  readonly type = CLOSE_SIDEBAR;
}

export class ToggleSidebarAction implements Action {
  readonly type = TOGGLE_SIDEBAR;
}

export class OpenModalAction implements Action {
  readonly type = OPEN_MODAL;
}

export class CloseModalAction implements Action {
  readonly type = CLOSE_MODAL;
}

export type Actions = OpenSidebarAction | CloseSidebarAction | ToggleSidebarAction | OpenModalAction | CloseModalAction;
