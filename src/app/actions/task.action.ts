import {Action} from '@ngrx/store';

export const ADD_TASK = '[Tasks] Add task';
export const REMOVE_TASK = '[Tasks] Remove task';
export const TOGGLE_TASK = '[Tasks] Toggle task';

export class AddTaskAction implements Action {
  readonly type = ADD_TASK;

  constructor(public payload: any) {
  }
}

export class RemoveTaskAction implements Action {
  readonly type = REMOVE_TASK;

  constructor(public payload: any) {
  }
}

export class ToggleTaskAction implements Action {
  readonly type = TOGGLE_TASK;

  constructor(public payload: any) {
  }
}

export type Actions = AddTaskAction | RemoveTaskAction | ToggleTaskAction;
