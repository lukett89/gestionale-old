import { SeriesType } from "./series-type.interface";
import { Store } from "./store.interface";

export interface StoreSettingItem {
  id: number,
  code: string,
  type: SeriesType[];
  store: Store;
}
