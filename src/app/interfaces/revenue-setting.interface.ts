import { StoreSettingItem } from "./store-setting-item.interface";
import { Category } from "./category.interface";

export interface RevenueSetting extends StoreSettingItem{
  hasAutoDecrease: boolean;
  categories: Category[];
  hasProductionCost: boolean;
  productionCostAmount: number;
  hasFixedRevenueAmount: boolean;
  revenueAmount: number;
  hasTaxes: boolean;
  taxAmount: number;
}
