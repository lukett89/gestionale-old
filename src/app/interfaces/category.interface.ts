import { Store } from "./store.interface";

export interface Category {
  id: number,
  name: string,
  shop: Store;
}
