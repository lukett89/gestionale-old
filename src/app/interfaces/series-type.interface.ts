export interface SeriesType {
  id: number,
  name: string
}
