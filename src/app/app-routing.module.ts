import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {StoreConfigurationComponent} from './pages/storeConfiguration/store-configuration.component';
import {HomeComponent} from './pages/home/home.component';

export const routes: Routes = [
  {path: '', redirectTo: '/home', pathMatch: 'full'},
  {
    path: 'home',
    component: HomeComponent,
  },
  {
    path: 'store-configuration',
    component: StoreConfigurationComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
  ],
  exports: [RouterModule],
  providers: [],
})
export class AppRoutingModule {
}
