import { createFeatureSelector, createSelector } from "@ngrx/store";
import { State } from "../reducers/task.reducer";
import * as fromTasks from '../reducers/task.reducer';

export const getTaskState = createFeatureSelector<fromTasks.State>('tasks');

export const getTasks = createSelector(getTaskState, (state: State) => state.tasks);
