import { createFeatureSelector, createSelector } from "@ngrx/store";
import { State } from "../reducers/layout.reducer";
import * as fromLayout from '../reducers/layout.reducer';

export const getLayoutState = createFeatureSelector<fromLayout.State>('layout');

export const getShowSidebar = createSelector(getLayoutState, (state: State) => state.showSidebar);
