import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {NgModule} from '@angular/core';
import {AppComponent} from './app.component';
import {SidebarComponent} from './components/sidebar/sidebar.component';
import {HeaderComponent} from './components/header/header.component';
import {StoreModule} from '@ngrx/store';
import {reducers, metaReducers} from './reducers';
import {LayoutStoreService} from './services/LayoutService/layout-store.service';
import {HomeComponent} from './pages/home/home/home.component';
import {HomeModule} from './pages/home/home.module';
import {MaterialModule} from './core/material/material.module';
import {CommonComponentsModule} from '@app/components/common/common-components.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {AppRoutingModule} from './app-routing.module';
import {StoreConfigurationModule} from '@app/pages/storeConfiguration/store-configuration.module';
import { RevenueComponent } from "./components/dialog/revenue/revenue.component";
import { DialogService } from "./services/DialogService/dialog.service";

@NgModule({
  imports: [
    // angular
    BrowserAnimationsModule,
    BrowserModule,
    FormsModule,
    MaterialModule,
    CommonComponentsModule,
    AppRoutingModule,
    StoreModule.forRoot(reducers, {metaReducers}),
    // page modules
    HomeModule,
    ReactiveFormsModule,
    StoreConfigurationModule,
  ],
  declarations: [
    AppComponent,
    HeaderComponent,
    SidebarComponent,
    RevenueComponent,
  ],
  entryComponents: [
    RevenueComponent
  ],
  providers: [
    LayoutStoreService,
    DialogService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
