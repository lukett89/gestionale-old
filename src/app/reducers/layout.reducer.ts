import * as layout from '../actions/layout.action';

export interface State {
  showSidebar: boolean;
  isModalOpen: boolean;
}

const initialState: State = {
  showSidebar: true,
  isModalOpen: false,
};

export function reducer(state = initialState, action: layout.Actions): State {
  switch (action.type) {
    case layout.CLOSE_SIDEBAR:
      return {...state,
        showSidebar: false
      };

    case layout.OPEN_SIDEBAR:
      return {...state,
        showSidebar: true
      };
    case layout.TOGGLE_SIDEBAR:
      return {...state,
        showSidebar: !state.showSidebar
      };

    case layout.OPEN_MODAL:
      return {...state,
        isModalOpen: true
      };

    case layout.CLOSE_MODAL:
      return {...state,
        isModalOpen: false
      };
    default:
      return state;
  }
}

