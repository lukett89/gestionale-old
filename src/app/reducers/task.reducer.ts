import * as task from '../actions/task.action';
import {Task} from '../interfaces/task.interface';

export interface State {
  tasks: Task[]
}

const initialState: State = {
  tasks: []
};

let id = 0;

export function reducer(state = initialState, action: task.Actions): State {
  switch (action.type) {

    case task.ADD_TASK:
      return {...state, tasks: [...state.tasks, {id: id++, text: action.payload, completed: false}]};

    case task.TOGGLE_TASK:
      return {...state, tasks: state.tasks.map(task => (task.id === action.payload)
        ? {...task, completed: !task.completed}
        : {...task}
      )};

    default:
      return state;
  }
}
