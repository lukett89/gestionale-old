import {Component, OnInit} from '@angular/core';
import {LayoutStoreService} from '../../services/LayoutService/layout-store.service';

@Component({
  selector: 'sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  constructor(public layoutStoreService: LayoutStoreService) {
  }

  ngOnInit() {
  }

}
