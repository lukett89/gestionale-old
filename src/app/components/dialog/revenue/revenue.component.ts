import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { SeriesType } from "../../../interfaces/series-type.interface";
import { Store } from "../../../interfaces/store.interface";
import { Category } from "../../../interfaces/category.interface";

@Component({
  selector: 'revenue',
  templateUrl: './revenue.component.html',
  styleUrls: ['./revenue.component.scss']
})
export class RevenueComponent implements OnInit {

  formGroup: FormGroup;

  revenueTypes: SeriesType[];

  availableStore: Store[];

  availableCategories: Category[];

  constructor(public dialogRef: MatDialogRef<RevenueComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any) {
  }

  ngOnInit() {
    this.formGroup = new FormGroup({
      id: new FormControl(0),
      code: new FormControl('', Validators.required),
      type: new FormControl(0, Validators.required),
      store: new FormControl(0, Validators.required),
      hasAutoDecrease: new FormControl(false),
      categories: new FormControl(0), // TODO disabled ?
      hasProductionCost: new FormControl(false),
      productionCostAmount: new FormControl({value: 0, disabled: true}),
      hasFixedRevenueAmount: new FormControl(false),
      revenueAmount: new FormControl({value: 0, disabled: true}),
      hasTaxes: new FormControl(false),
      taxAmount: new FormControl({value: 0, disabled: true}),
    });
    // TODO read from the model
    this.revenueTypes = [{id: 0, name: 'Occasionale'}, {id: 1, name: 'Serie'}];
    this.availableStore = [{id: 0, name: 'Store'}, {id: 1, name: 'Sito'}];
    this.availableCategories = <Category[]>[{id: 0, name: 'Anelli'}, {id: 1, name: 'Bracciali'}];
  }

  public disableControl(controlName: string, value: boolean) {
    if (value) {
      this.formGroup.controls[controlName].disable();
    } else {
      this.formGroup.controls[controlName].enable();
    }
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
