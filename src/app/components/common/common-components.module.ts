import {NgModule} from '@angular/core';
import {CustomCardComponent} from './custom-card/custom-card.component';
import {MaterialModule} from '../../core/material/material.module';
import {FormsModule} from '@angular/forms';
import {BrowserModule} from '@angular/platform-browser';

@NgModule({
  imports: [
    MaterialModule,
    BrowserModule,
    FormsModule,
  ],
  declarations: [
    CustomCardComponent
  ],
  exports: [
    CustomCardComponent
  ],
  providers: []
})

export class CommonComponentsModule {}
