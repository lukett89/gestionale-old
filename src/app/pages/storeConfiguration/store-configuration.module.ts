import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {BrowserModule} from '@angular/platform-browser';
import {StoreConfigurationComponent} from './store-configuration.component';
import {MaterialModule} from '@app/core/material';
import {CommonComponentsModule} from '@app/components/common/common-components.module';

@NgModule({
  imports: [
    BrowserModule,
    CommonComponentsModule,
    FormsModule,
    MaterialModule,
  ],
  exports: [
    StoreConfigurationComponent
  ],
  declarations: [
    StoreConfigurationComponent,
  ],
  providers: [],
})
export class StoreConfigurationModule {
}
