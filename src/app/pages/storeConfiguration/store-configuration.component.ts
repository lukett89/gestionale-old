import { Component, OnInit } from '@angular/core';
import { DialogService } from "../../services/DialogService/dialog.service";
import { RevenueComponent } from "../../components/dialog/revenue/revenue.component";

@Component({
  selector: 'store-configuration',
  templateUrl: './store-configuration.component.html',
  styleUrls: ['./store-configuration.component.scss']
})
export class StoreConfigurationComponent implements OnInit {

  constructor(public dialogService: DialogService) { }

  ngOnInit() {
  }

  openRevenueDialog() {
    this.dialogService.openDialog(RevenueComponent, null);
  }

  openCostDialog() {

  }
}
