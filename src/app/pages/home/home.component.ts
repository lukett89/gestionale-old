import {Component, OnInit} from '@angular/core';
import {TaskManagerService} from '../../services/TaskManager/task-manager.service';

@Component({
  selector: 'home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  taskValue: string;

  priority: number;

  priorityValues = [1, 2, 3];

  categoryValues = ['Ring', 'T-shirt'];

  expensesValues = ['Single', 'Series'];

  constructor(public taskManagerService: TaskManagerService) {
  }

  ngOnInit() {
  }

}
