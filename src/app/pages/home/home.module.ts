import {NgModule} from '@angular/core';
import {HomeComponent} from './home.component';
import {CommonComponentsModule} from '@app/components/common/common-components.module';
import {MaterialModule} from '@app/core/material';
import {FormsModule} from '@angular/forms';
import {TaskManagerService} from '../../services/TaskManager/task-manager.service';
import {BrowserModule} from '@angular/platform-browser';

@NgModule({
  imports: [
    BrowserModule,
    CommonComponentsModule,
    FormsModule,
    MaterialModule
  ],
  exports: [
    HomeComponent
  ],
  declarations: [
    HomeComponent
  ],
  providers: [
    TaskManagerService
  ],
})
export class HomeModule {
}
