import { Injectable, TemplateRef } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ComponentType } from "@angular/cdk/portal";
import { Store } from "@ngrx/store";
import * as fromRoot from '../../reducers';
import { CloseModalAction, OpenModalAction } from "../../actions/layout.action";

@Injectable()
export class DialogService {

  constructor(public dialog: MatDialog, private _store: Store<fromRoot.State>) {
  }

  openDialog(component: ComponentType<any> | TemplateRef<any>, data: any) {
    this._store.dispatch(new OpenModalAction());
    const dialogRef = this.dialog.open(component, {
      width: this.computeModalWidth(),
      data: data
    });

    dialogRef.afterClosed().subscribe(result => {
      this._store.dispatch(new CloseModalAction());
    });
  }

  private computeModalWidth(): string {
    return window.innerWidth < 768 ? window.innerWidth + 'px' : (window.innerWidth * 60 / 100) + 'px';
  }
}
