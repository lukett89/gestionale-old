import { TestBed, inject } from '@angular/core/testing';

import { LayoutStoreService } from './layout-store.service';

describe('LayoutStoreService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LayoutStoreService]
    });
  });

  it('should be created', inject([LayoutStoreService], (service: LayoutStoreService) => {
    expect(service).toBeTruthy();
  }));
});
