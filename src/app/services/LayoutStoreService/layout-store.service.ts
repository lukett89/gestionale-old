import { Injectable } from '@angular/core';
import * as fromRoot from '../../reducers'
import {Store} from "@ngrx/store";
import { ToggleSidebarAction } from "../../actions/layout.action";
import { Observable } from 'rxjs';
import { getShowSidebar } from "../../selectors/layout.selectors";

@Injectable()
export class LayoutStoreService {

  private readonly _showSidebar$: Observable<boolean>;

  constructor(private _store: Store<fromRoot.State>) {
    this._showSidebar$ = _store.select(getShowSidebar)
  }

  public toggle() {
    this._store.dispatch(new ToggleSidebarAction());
  }

  get showSidebar$() {
    return this._showSidebar$;
  }
}
