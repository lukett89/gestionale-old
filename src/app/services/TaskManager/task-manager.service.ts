import {Injectable} from '@angular/core';
import * as fromRoot from '../../reducers';
import {Store} from '@ngrx/store';
import {Observable} from 'rxjs';
import {getTasks} from '../../selectors/task.selector';
import {AddTaskAction, ToggleTaskAction} from '../../actions/task.action';
import {Task} from '../../interfaces/task.interface';

@Injectable()
export class TaskManagerService {

  private readonly _tasks$: Observable<Task[]>;

  constructor(private _store: Store<fromRoot.State>) {
    this._tasks$ = _store.select(getTasks);
  }

  public addTask(task: string) {
    if(!!task && task!=='') {
      this._store.dispatch(new AddTaskAction(task));
    }
  }

  public toggleTask(id: number) {
    this._store.dispatch(new ToggleTaskAction(id));
  }

  public getTasks(task: string): Observable<Task[]> {
    return this._tasks$;
  }
}
